# Función para agregar archivos txt a la bd

requiere [python3]

## Formato del archivo de texto

category: Favoritos Trabajadores

subcat: texto  
Que es mi cuenta infonavit  
En que consiste mi cuenta infonavit  
Respuesta:  
Es un servicio dentro del Portal de Internet cuyo objetivo es proporcionar servicios e información de acuerdo a las características, situación o necesidades de la persona que accede, bajo un esquema de seguridad con el uso de un correo electrónico y contraseña.

subcat: Registro en Mi Cuenta Infonavit  
que debo hacer para registrarme en mi cuenta infonavit  
como me registro en mi cuenta infonavit  
en donde me registro para estar en mi cuenta infonavit  
Respuesta:  
Si ya iniciaste el trámite de tu crédito o eres uno de nuestros acreditados, puedes registrarte en Mi Cuenta Infonavit, en la sección de Trabajadores del portal, con sólo ingresar tu Número de Seguridad Social (NSS), tu CURP y tu RFC. Luego deberás proporcionar un correo electrónico y una contraseña.
Respuesta:  
Si te registras en Mi Cuenta Infonavit podrás consultar, solicitar o recibir en tu correo electrónico toda la información que necesitas saber sobre tu crédito.

category: Favoritos Trabajadores
...


Añadir accesos a la bd

```
DB_HOST = '' 
DB_USER = '' 
DB_PASS = '' 
DB_NAME = ''
```
Modificar el id del product container

```
product_container = 0
```

Agregar el nombre del archivo y colocar el txt en la misma carpeta

```
file = ''
```

Ejecutar
```
python3 infonavit.py
```
