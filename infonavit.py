# coding utf-8
import codecs
import MySQLdb

#acceso a la bd
DB_HOST = '' 
DB_USER = '' 
DB_PASS = '' 
DB_NAME = ''

#configuración product container
file = ''
category = 'category: '
subcat = 'subcat: '
respuesta = 'Respuesta:'
product_container = 0
next_item = ''
st_answer = ''

def run_query(query=''): 
    datos = [DB_HOST, DB_USER, DB_PASS, DB_NAME] 
 
    conn = MySQLdb.connect(*datos) # Conectar a la base de datos 
    cursor = conn.cursor()         # Crear un cursor 
    cursor.execute(query)          # Ejecutar una consulta 
 
    if query.upper().startswith('SELECT'): 
        data = cursor.fetchall()   # Traer los resultados de un select 
    else: 
        conn.commit()              # Hacer efectiva la escritura de datos 
        data = cursor.lastrowid
 
    cursor.close()                 # Cerrar el cursor 
    conn.close()                   # Cerrar la conexión 
 
    return data

file=codecs.open(file,'r','utf-8')

for line in file:
  if line.startswith(category):
    query = "INSERT INTO category VALUES (NULL,%i, '%s', '%s')" % (product_container, line[len(category):-1], line[len(category):-1])
    id_category = run_query(query)
    #print('insercion categoria')
    next_item = 'Subcategoria'
  elif line.startswith(subcat):
    if len(st_answer) > 0:
      #print('insercion respuesta')
      query = "INSERT INTO answer VALUES (NULL, %i, 1, '%s', 1)" % (id_subcategory, st_answer)
      query_answer = run_query(query)
      st_answer = ''
    if len(line[len(subcat):-1]) > 45:
      line = line[len(subcat):len(subcat)+45]
    else:
      line = line[len(subcat):-1]
    query = "INSERT INTO sub_category VALUES (NULL, %i, '%s', '%s')" % (id_category, line, line)
    id_subcategory = run_query(query)
    print('id: ', id_subcategory,'descripcion: ', line)
    next_item = 'Pregunta'
  elif line.startswith(respuesta):
    if len(st_answer) > 0:
      #print('insercion respuesta')
      query = "INSERT INTO answer VALUES (NULL, %i, 1, '%s', 1)" % (id_subcategory, st_answer)
      query_answer = run_query(query)
      st_answer = ''
    next_item = 'Respuesta'
  elif not line.startswith('\n') and next_item == 'Pregunta':
    query = "INSERT INTO question VALUES (NULL, %i, 1, '%s', 1)" % (id_subcategory, line[:-1])
    pregunta = run_query(query)
    #print('insercion pregunta')
  elif not line.startswith('\n') and next_item == 'Respuesta':
    st_answer = st_answer + line
file.close()

if len(st_answer) > 0:
  query = "INSERT INTO answer VALUES (NULL, %i, 1, '%s', 1)" % (id_subcategory, st_answer)
  query_answer = run_query(query)